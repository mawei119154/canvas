class WrappedCanvas {
  constructor(canvas) {
    this.ctx = canvas.getContent("2d");
    this.width = canvas.width;
    this.height = canvas.height;
    this.executionArray = [];
  }
  drawImage(...params) {
    this.executionArray.push({
      method: "drawImage",
      params: params
    });
    this.ctx.drawImage(...params);
  }
  clearCanvas() {
    this.ctx.clearRect(0, 0, this.width, this.height);
  }
  undo() {
    if (this.executionArray.length > 0) {
      //   清空画布
      this.clearCanvas();
      // 删除当前操作
      this.executionArray.pop();
      // 逐个执行绘图动作进行重绘
      for (const iterator of this.executionArray) {
        this.ctx[iterator.method](...iterator.params);
      }
    }
  }
}

export default WrappedCanvas;
