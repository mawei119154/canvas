/**
 * 判断数据类型
 * @param {*} data
 */
export const type = s =>
  Object.prototype.toString
    .call(s)
    .slice(8, -1)
    .toLowerCase();
[
  "Array",
  "Undefined",
  "Null",
  "Boolean",
  "Number",
  "Function",
  "String",
  "Symbol",
  "Object"
].forEach(v => (type["is" + v] = s => type(s) === v.toLowerCase()));
