import { type } from "../untils/dataType";
class ElementResize {
  constructor(eleSelector) {
    if (!eleSelector) return;
    this.eleSelector = eleSelector;
    this.el = document.querySelector(eleSelector);
    this.queue = [];
    this._init();
    window.ElementResize = ElementResize;
  }
  _init() {
    let iframe = this.crateIElement();
    this.el.style.position = "relative";
    this.el.appendChild(iframe);
    this.bindEvent(iframe.contentWindow);
  }
  crateIElement() {
    let iframe = document.createElement("iframe");
    this.setStyle(iframe);
    return iframe;
  }
  setStyle(el, styleJson) {
    if (!el) return;
    styleJson = styleJson || {
      opacity: 0,
      "z-index": "-111",
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      border: 0
    };
    let styleText = "";
    for (const key in styleJson) {
      styleText += key + ":" + styleJson[key] + ";";
    }
    el.style.cssText = styleText;
  }
  bindEvent(el) {
    if (!el) return;
    el.addEventListener(
      "resize",
      e => {
        this.runQueue(e.target.innerWidth, e.target.innerHeight);
      },
      false
    );
  }
  runQueue(w, h) {
    let queue = this.queue;
    for (let index = 0; index < queue.length; index++) {
      typeof queue[index] === "function" && queue[index].apply(this, [w, h]);
    }
  }
  listen(cb) {
    if (!type.isFunction(cb)) throw new TypeError("cb is not a function");
    this.queue.push(cb);
  }
  unBindEvent() {
    if (!this.el) return;
    this.el.removeEventListener(
      "resize",
      () => {
        return "解除绑定";
      },
      false
    );
  }
}

export default ElementResize;
